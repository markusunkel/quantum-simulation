/* 
The MIT License (MIT)

Copyright (c) 2016 Markus Unkel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
function Plotting(container, width, height, clearColor) {
    this.container = container;
    this.width = width;
    this.height = height;
    this.clearColor = clearColor || 0xFFFFFF;
    this._running = false;

    // CANVAS
    var renderer = PIXI.autoDetectRenderer(width, height, {antialias: true});
    renderer.backgroundColor = clearColor || 0xFFFFFF;
    this._renderer = renderer;

    // Setup HTML
    var DOM = document.getElementById(container);
    $("#"+container).empty();
    DOM.appendChild(this._renderer.view);
    this._DOM = DOM;

    return this;
}
/*  RUN
*   ----------------------------------
*   1. define targetFPS
*   2. animation loop -> frameCall()
*   3. fps control: 
*       if fps < targetFPS/2
*         disable HQ
*/  
var foundResonances = new Array();
Plotting.prototype.run = function() {
  this._running = true;
  var self = this;
  var now;
  var fps = 30;
  var then = Date.now();
  var interval = 1000/fps;
  var delta;
  var fpsLength = 50;
  var fpsHistory = [];
  for (var i = 0; i < fpsLength; i++)
    fpsHistory.push(0.0);
  var fpsElem = 0;
  var fpsAver = 0;
  var fpsCrit = interval*2;
  // resonance routine parameters
  var lastProb = 0.0;
  var maxProb;
  var firstMaxProb = false;
  var resonanceTestCounter = 0;

  var animation = function () {
    now = Date.now();
    delta = now - then;
    if (!self._running) {
      return;
    }
    // resonance routine
    /*
    if (delta > interval) {
      // #################
      // resonance control
      // get sum of abs2 of wavefunction between 5 <= x <= 10
      var eKin = plotting._quantum._config.wavefunction.energy;
      var prob = 0.0;
      var wave = plotting._quantum._wavepacket;
      for (var i = 4368; i < 4643; i++) {
        prob += wave[i].abs2();
      }
      var checkout = false;
      if (lastProb > 1 && prob < lastProb) {
        // set maximum prob
        if (!firstMaxProb) {
          firstMaxProb = true;
          maxProb = lastProb;
        }
        // check if prob is relativ 0.01 away from maxProb
        if (maxProb - prob < 0.005 * maxProb){
          resonanceTestCounter += 1;
        }
        else {
          //console.log("push");
          foundResonances.push([eKin, resonanceTestCounter]);
          checkout = true;
        }
        // routine to find next energy resonance
        if (checkout) {
          firstMaxProb = false;
          lastProb = 0.0;
          resonanceTestCounter = 0;
          // routine to find next energy resonance
          if(eKin >= 70) {
            var newEkin = eKin - 0.1;
            console.log("E_Kin: " + newEkin);
            self._running = false;
            plotting._quantum._config.wavefunction.energy = newEkin;
            plotting.buildUp();
            plotting.newExpTPValues();
            return;
          }
          else {
            // plot array
            var result = "ekin, 0.003fs<br/>";
            for (var i = 0; i < foundResonances.length; i++) {
              result += foundResonances[i][0] + " , " + foundResonances[i][1] + "<br/>";
            }
            $("body").html(result);
            return;
          }
        }
      }
      //console.log("LastProb: "+lastProb+", prob: ",+prob+", maxProb: "+ maxProb+", firstMaxProb: "+firstMaxProb+", resonanceTestCounter: "+ resonanceTestCounter);
      lastProb = prob;
    }
    */
    requestAnimationFrame(animation);
    if (delta > interval) {
      // fps control
      if (fpsControl) {
        fpsElem = fpsElem % fpsLength;
        fpsHistory[fpsElem] = delta;
        fpsElem += 1;
        fpsAver = 0.0;
        for (var i = 0; i < fpsLength; i++)
          fpsAver += fpsHistory[i];
        fpsAver /= fpsLength;
        if(fpsAver > fpsCrit) {
          // do hq off
          var sampling = plotting._quantum._config.length;
          if (sampling > 2048) {
            $("#fps-snackbar, #high-quality").trigger("click");
          }
        }
      }
      then = now - (delta % interval);
      self.frameCall();
    }
  };
  requestAnimationFrame(animation);
};
/*  FRAMECALL
*   ----------------------------------
*   1. do quantum.Simulation()
*   2. update time
*   3. render scene
*/
Plotting.prototype.frameCall = function() {
  // make simulation
  this._quantum.Simulation();
  // update time
  $('#time span').html(this._time.toExponential(2));
  // render frame
  this._renderer.render(this._scene);  
};
/*  BUILDUP
*   ----------------------------------------------------------
*   1. define control panel icons
*   2. t = 0 and stop previous animation
*   3. set screen width specific design
*   4. create wavefunc and potential: quantum.InitialPhysics()
*   5. draw x-y-label + physics: quantum.BuildUp()
*   6. first render of scene
*/
Plotting.prototype.buildUp = function() {
  // reset video control panel
  var inactiveColor = 'mdl-color-text--blue-grey-200';
  var activeColor = 'mdl-color-text--indigo-500';
  var inactiveClass = 'video-control-inactive';
  var activeClass = 'video-control-active';
  var videoIcons = '#stop i, #pause i, #playArrow i, #fast_forward i, #fast_rewind i';
  $(videoIcons).removeClass(activeColor + ' ' + activeClass).addClass(inactiveColor + ' ' + inactiveClass);
  $('#stop i').removeClass(inactiveColor + ' ' + inactiveClass).addClass(activeClass + ' ' + activeColor);
  // set time to zero
  this._time = 0.00;
  $('#time span').html(this._time.toExponential(2));
  // stop run
  this.stop();
  // 
  var width = $( window ).width();
  if (width <= 530) {
    this._quantum._design.xLabel.plot = false;
    this._quantum._design.yLabel.plot = false;
    this._quantum._config.momentum = false;
    $("#border_left i, #border_bottom i, #kRoomOption i").removeClass(activeColor + ' ' + activeClass).addClass(inactiveColor + ' ' + inactiveClass);
    var border = '1px';
    $('#animation-container').css("border-bottom-width", border);
  }
  if(this._quantum._design.xLabel.plot) {
    var border = '0px';
    $('#animation-container').css("border-bottom-width", border);
  }
  // define firstBild
  if(!this._firstBuild) {
    this._firstBuild = true;
    this._quantum._plotting = this;
    this.VideoControlPanel();
  }
  // create potential and wavefunction
  this._quantum.InitialPhysics();
  // draw 
  this._quantum.BuildUp();

  // render buildUp
  this._renderer.render(this._scene); 
};

Plotting.prototype.stop = function () {
  // stop run animation loop, last frame remains
  this._running = false;
};

Plotting.prototype.play = function () {
  if (!this._running){
    this.run();
  }
  else {
    this._running = true;
  }
};
/*  NEWEXPTPVALUES
*   ---------------------------------------------------
*   1. precompute expTP values of split operator method
*   2. stop -> play animation
*/
Plotting.prototype.newExpTPValues = function () {
  this._quantum.expTPValues();
  this.play();
};
/*  SETTINGCARD
*   ----------------------------------------------------
*   input, output, general behaviour of control settings
*/
Plotting.prototype.SettingsCard = function () {
  var self = this;
  var plotting = this;
  var design = this._quantum._design;
  var config = this._quantum._config;
  $("#settings-panel input, #psi-panel input, #potential-panel input").unbind();
  $("#settings-panel input, #psi-panel input, #potential-panel input").val("");
  // fill setting panel
  // xmin/max
  $("#settings-panel-def-xrange input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", config.grid.xmax-config.grid.xmin);
     }, 110);
  });
  $("#settings-panel-def-xrange input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val) || parseInt(val) <= 2 ) {
      $("#settings-panel-def-xrange span").addClass('is-invalid');
    }
    else if(val) {
      $("#settings-panel-def-xrange span").removeClass('is-invalid');
      self._quantum._config.grid.xmin = -parseInt(val/2);
      self._quantum._config.grid.xmax = parseInt(val/2);
      self.buildUp();
    }
  });
  $('#settings-panel-def-xrange input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val) || parseInt(val) <= 2 ) {
        $("#settings-panel-def-xrange span").addClass('is-invalid');
      }
      else if(val) {
        $("#settings-panel-def-xrange span").removeClass('is-invalid');
        self._quantum._config.grid.xmin = -parseInt(val/2);
        self._quantum._config.grid.xmax = parseInt(val/2);
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // yrange = maxEnergy on PlotHeightlevel 0.618 Golden Ratio
  $("#settings-panel-def-yrange input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", config.maxEnergy);
     }, 110);
  });
  $("#settings-panel-def-yrange input").focusout(function (){
    var val = $(this).val();
    $(this).attr("placeholder", "");
    if (isNaN(val) || parseInt(val) < 0) {
      $("#settings-panel-def-yrange span").addClass('is-invalid');
    }
    else if(val) {
      $("#settings-panel-def-yrange span").removeClass('is-invalid');
      if (parseInt(val)==0) {
        self._quantum._config.maxEnergy = false;
      }
      else {
        self._quantum._config.maxEnergy = val * 0.618;
      }
      self.buildUp();
    }
  });
  $('#settings-panel-def-yrange input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val) || parseInt(val) < 0) {
        $("#settings-panel-def-yrange span").addClass('is-invalid');
      }
      else if(val) {
        $("#settings-panel-def-yrange span").removeClass('is-invalid');
        if (parseInt(val)==0) {
          self._quantum._config.maxEnergy = false;
        }
        else {
          self._quantum._config.maxEnergy = val * 0.618;
        }
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // deltax
  $("#settings-panel-def-deltax input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", design.xLabel.delta);
     }, 110);
  });
  $("#settings-panel-def-deltax input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val)) {
      $("#settings-panel-def-deltax span").addClass('is-invalid');
    }
    else if(val) {
      $("#settings-panel-def-deltax span").removeClass('is-invalid');
      self._quantum._design.xLabel.delta = parseInt(val);
      self.buildUp();
    }
  });
  $('#settings-panel-def-deltax input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val)) {
        $("#settings-panel-def-deltax span").addClass('is-invalid');
      }
      else if(val) {
        $("#settings-panel-def-deltax span").removeClass('is-invalid');
        self._quantum._design.xLabel.delta = parseInt(val);
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // deltay
  $("#settings-panel-def-deltay input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", design.yLabel.sampling);
     }, 110);
  });
  $("#settings-panel-def-deltay input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val) || parseInt(val) < 1) {
      $("#settings-panel-def-deltay").addClass('is-invalid');
    }
    else if(val) {
      $("#settings-panel-def-deltay").removeClass('is-invalid');
      self._quantum._design.yLabel.sampling = parseInt(val);
      self.buildUp();
    }
  });
  $('#settings-panel-def-deltay input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val) || parseInt(val) < 1) {
        $("#settings-panel-def-deltay").addClass('is-invalid');
      }
      else if(val) {
        $("#settings-panel-def-deltay").removeClass('is-invalid');
        self._quantum._design.yLabel.sampling = parseInt(val);
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // sampling points
  $("#settings-panel-def-sampl input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", Math.log2(config.length));
     }, 110);
  });
  $("#settings-panel-def-sampl input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val) || parseInt(val) < 11) {
      $("#settings-panel-def-sampl").addClass('is-invalid');
    }
    else if(val){
      $("#settings-panel-def-sampl").removeClass('is-invalid');
      self._quantum._config.length = Math.pow(2,parseInt(val));
      self.buildUp();
      var inactiveColor = 'mdl-color-text--blue-grey-200';
     var activeColor = 'mdl-color-text--indigo-500';
      var inactiveClass = 'video-control-inactive';
      var activeClass = 'video-control-active';
      if(parseInt(val)<13)
        $('#high-quality i').removeClass(activeClass + ' ' + activeColor).addClass(inactiveColor + ' ' + inactiveClass);   
      else
        $('#high-quality i').removeClass(inactiveColor + ' ' + inactiveClass).addClass(activeClass + ' ' + activeColor);  
    }
  });
  $('#settings-panel-def-sampl input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val) || parseInt(val) < 11) {
        $("#settings-panel-def-sampl").addClass('is-invalid');
      } else if(val){
        $("#settings-panel-def-sampl").removeClass('is-invalid');
        self._quantum._config.length = Math.pow(2,parseInt(val));
        self.buildUp();
        var inactiveColor = 'mdl-color-text--blue-grey-200';
       var activeColor = 'mdl-color-text--indigo-500';
        var inactiveClass = 'video-control-inactive';
        var activeClass = 'video-control-active';
        if(parseInt(val)<13)
          $('#high-quality i').removeClass(activeClass + ' ' + activeColor).addClass(inactiveColor + ' ' + inactiveClass);   
        else
          $('#high-quality i').removeClass(inactiveColor + ' ' + inactiveClass).addClass(activeClass + ' ' + activeColor);  
      }
      $(this).blur();    
    }
  });

  // wave packet
  // energy
  $("#psi-panel-energy input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", config.wavefunction.energy);
     }, 110);
  });
  $("#psi-panel-energy input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (val){
      var en = parseFloat(val);
      var k0 = Math.sqrt(en*2*config.wavefunction.mass/(H_RED*H_RED)-1/(2*config.wavefunction.sigma*config.wavefunction.sigma));
    if (isNaN(k0) || isNaN(val) ||parseFloat(val) < 0) {
      console.log("JO", en);
      $("#psi-panel-energy").addClass('is-invalid');
    }
    else if(val) {
      $("#psi-panel-energy").removeClass('is-invalid');
      config.wavefunction.energy = parseFloat(val);
      self.buildUp();
    }
  }
  });
  $('#psi-panel-energy input').keypress(function(e){
    if(e.which == 13){
      $(this).trigger("focusout");
      $(this).blur();    
    }
  });
  // k0
  $("#psi-panel-k0 input").focusin(function (){
    var self = this;
    var k0 = Math.sqrt(config.wavefunction.energy*2*config.wavefunction.mass/(H_RED*H_RED)-1/(2*config.wavefunction.sigma*config.wavefunction.sigma));
    setTimeout(function() {
      $(self).attr("placeholder", k0);
     }, 110);
  });
  $("#psi-panel-k0 input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    var energy = H_RED*H_RED/(2*config.wavefunction.mass)*(val*val + 1/(2*config.wavefunction.sigma*config.wavefunction.sigma));
    if (isNaN(val) ||parseFloat(val) < 0) {
      $("#psi-panel-k0").addClass('is-invalid');
    }
    else if(val) {
      $("#psi-panel-k0").removeClass('is-invalid');
      config.wavefunction.energy = parseFloat(energy);
      self.buildUp();
    }
  });
  $('#psi-panel-k0 input').keypress(function(e){
    if(e.which == 13){
      $(this).trigger("focusout");
      $(this).blur();    
    }
  });
  // masse
  $("#psi-panel-mass input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", config.wavefunction.mass/M_EL);
     }, 110);
  });
  $("#psi-panel-mass input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val) || parseFloat(val) <= 0) {
      $("#psi-panel-mass").addClass('is-invalid');
    }
    else if(val) {
      $("#psi-panel-mass").removeClass('is-invalid');
      config.wavefunction.mass = val*M_EL;
      self.buildUp();
    }
  });
  $('#psi-panel-mass input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val) || parseFloat(val) <= 0) {
        $("#psi-panel-mass").addClass('is-invalid');
      }
      else if(val) {
        $("#psi-panel-mass").removeClass('is-invalid');
        config.wavefunction.mass = val*M_EL;
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // sigma
  $("#psi-panel-sigma input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", config.wavefunction.sigma);
     }, 110);
  });
  $("#psi-panel-sigma input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val) || parseFloat(val) <= 0) {
      $("#psi-panel-sigma").addClass('is-invalid');
    }
    else if(val) {
      $("#psi-panel-sigma").removeClass('is-invalid');
      config.wavefunction.sigma = parseFloat(val);
      self.buildUp();
    }
  });
  $('#psi-panel-sigma input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val) || parseFloat(val) <= 0) {
        $("#psi-panel-sigma").addClass('is-invalid');
      }
      else if(val) {
        $("#psi-panel-sigma").removeClass('is-invalid');
        config.wavefunction.sigma = parseFloat(val);
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // x0
  $("#psi-panel-x0 input").focusin(function (){
    var self = this;
    setTimeout(function() {
      $(self).attr("placeholder", config.wavefunction.x0);
     }, 110);
  });
  $("#psi-panel-x0 input").focusout(function (){
    $(this).attr("placeholder", "");
    var val = $(this).val();
    if (isNaN(val)) {
      $("#psi-panel-x0").addClass('is-invalid');
    }
    else if(val) {
      $("#psi-panel-x0").removeClass('is-invalid');
      config.wavefunction.x0 = parseFloat(val);
      self.buildUp();
    }
  });
  $('#psi-panel-x0 input').keypress(function(e){
    if(e.which == 13){
      $(this).attr("placeholder", "");
      var val = $(this).val();
      if (isNaN(val)) {
        $("#psi-panel-x0").addClass('is-invalid');
      }
      else if(val) {
        $("#psi-panel-x0").removeClass('is-invalid');
        config.wavefunction.x0 = parseFloat(val);
        self.buildUp();
      }
      $(this).blur();    
    }
  });
  // ===============
  // Potential Panel
  // ===============
  // make pot type list
  function makePotTypeList() {
      // id:type
      // 0: rect
      // 1: osci
      // 2: gauss
      // 3: individuell
      $('#potential-panel-content-type ul').empty();
      var li = document.createElement('LI');
          li.setAttribute('class', 'mdl-menu__item');
          li.innerHTML = "<span class='mdl-color-text--blue-grey-600' style='text-transform:none;'>Individuell</span>";
          li.button = $("#potential-panel-content-type button");
          li.setAttribute("id", "type_1003");
      $('#potential-panel-content-type ul').append(li);
      var li = document.createElement('LI');
          li.setAttribute('class', 'mdl-menu__item');
          li.innerHTML = "<span class='mdl-color-text--blue-grey-600' style='text-transform:none;'>Rechteck</span>";
          li.button = $("#potential-panel-content-type button");
          li.setAttribute("id", "type_1000");
      $('#potential-panel-content-type ul').append(li);
      var li = document.createElement('LI');
          li.setAttribute('class', 'mdl-menu__item');
          li.innerHTML = "<span class='mdl-color-text--blue-grey-600' style='text-transform:none;'>Oszillator</span>";
          li.button = $("#potential-panel-content-type button");
          li.setAttribute("id", "type_1001");
      $('#potential-panel-content-type ul').append(li);
      var li = document.createElement('LI');
          li.setAttribute('class', 'mdl-menu__item');
          li.innerHTML = "<span class='mdl-color-text--blue-grey-600' style='text-transform:none;'>Gauss</span>";
          li.button = $("#potential-panel-content-type button");
          li.setAttribute("id", "type_1002");
      $('#potential-panel-content-type ul').append(li);
      // pot type list click events

  }
  // make potential list
  function makePotList() {
      $('#potential-panel-list ul').empty();
      var li = document.createElement('LI');
          li.setAttribute('class', 'mdl-menu__item');
          li.innerHTML = "<span class='mdl-color-text--blue-grey-600' style='text-transform:none;'><i class='material-icons' style='font-size:12px;margin-right:5px;'>add</i>Potential hinzufügen</span>";
          li.button = $("#potential-panel-list button");
          li.setAttribute("id", "pot_-1");
      $('#potential-panel-list ul').append(li);
    var potential = self._quantum._config.potential;
    for (var index in potential) {
      var li = document.createElement('LI');
          li.setAttribute('class', 'mdl-menu__item');
          li.innerHTML = "<span class='mdl-color-text--blue-grey-600' style='text-transform:none;'><i class='material-icons' style='font-size:12px;margin-right:5px;'>create</i>" + potential[index].name + "</span>";
          li.button = $("#potential-panel-list button");
          li.setAttribute("id", "pot_"+index);
      $('#potential-panel-list ul').append(li);
    }
    $("#potential-panel-list ul li, #potential-panel-content-type ul li").on("click",function () {
      $("#potential-panel-content-type ul li").unbind("click");
      var potential = config.potential;
      this.button.innerHTML = this.innerHTML;
      var id = $(this).attr("ID").split("_")[1];
      var divVal = "#pot-panel-val-";
      // val 1-4 labels -> "", unbind all val
      var allDivVal = $(divVal+"1 label, "+divVal+"2 label, "+divVal+"3 label, "+divVal+"4 label, "+divVal+"5 label, "+divVal+"6 label");
      allDivVal.html("");
      allDivVal.attr("placeholder", "");
      $(divVal+"1, "+divVal+"2 , "+divVal+"3, "+divVal+"4, "+divVal+"5, "+divVal+"6").hide();
      $(divVal+"1 input, "+divVal+"2 input, "+divVal+"3 input, "+divVal+"4 input, "+divVal+"5 input, "+divVal+"6 input, #potential-panel-content-title form input").unbind("focusin, focusout");
      $(divVal+"1 input, "+divVal+"2 input, "+divVal+"3 input, "+divVal+"4 input, "+divVal+"5 input, "+divVal+"6 input, #potential-panel-content-title form input").unbind("keypress");
      $(divVal+"1 input, "+divVal+"2 input, "+divVal+"3 input, "+divVal+"4 input, "+divVal+"5 input, "+divVal+"6 input").val("");
      if (id == -1) {
        // add new potential content
        // display pot type list
        $("#potential-panel-content-delete").addClass('displayNone');
        $("#potential-panel-content-type, #potential-panel-content-title, #potential-panel-content-potential-type").removeClass('displayNone');
        $("#potential-panel-content-title form input").val("Neu Rechteck");
        // add new potential slot
        potential.push({});
        id = potential.length-1;
        potential[id].type = 'Rect';
        potential[id].name = 'Neu Rechteck';
        potential[id].energy = 100;
        potential[id].width = 10;
        potential[id].x0 = 0;
        potential[id].alpha = 0.1;
        self.buildUp();
        makePotList();
      } else if(id < 1000) {
        $("#potential-panel-content-title, #potential-panel-content-delete, #potential-panel-content-potential-type").removeClass('displayNone');
        $("#potential-panel-content-type").addClass('displayNone');
        $("#potential-panel-content-title form input").val(config.potential[id].name);
      } else {
        if (id == 1000){
          potential[potential.length-1] = {
            type: 'Rect',
            name: 'Neues Rechteck',
            energy: 100,
            width: 10,
            x0: 0,
            alpha: 0.1
          };
        } else if (id == 1001){
          potential[potential.length-1] = {
            type: 'Osci',
            name: 'Neuer Oszillator',
            w: 0.5,
            x0: 0,
            xMin: -10,
            xMax: 10
          };
        } else if (id == 1002){
          potential[potential.length-1] = {
            type: 'Gauss',
            name: 'Neuer Gauss',
            energy: 100,
            sigma: 1,
            x0: 0
          };
        } else if (id == 1003){
          potential[potential.length-1] = {
            type: 'Personal',
            name: 'Individuell',
            potFunc: '100*Math.sin(x)',
            xMin: -10,
            xMax: 10
          };
        }
        id = potential.length-1;
        self.buildUp();
        $("#potential-panel-content-title form input").val(config.potential[id].name);
        makePotList();
      }
        // make type specific content
        var potConfig = potential[id];
        var potType = potConfig.type;
        // change name of potConfig
          $("#pot_choose_name").focusout(function (){
            var val = $(this).val();
            if(val) {
              potConfig.name = val;
              self.buildUp();
              makePotList();
            }
          });
          $("#pot_choose_name").keypress(function(e){
            if(e.which == 13){
              var val = $(this).val();
              if(val) {
                potConfig.name = val;
                self.buildUp();
                makePotList();

              }
              $(this).blur();    
            }
          });
        // types: Rect, Gauss, Osci
        // make labels of input and connections between input values and configs
        if (potType == 'Rect') {
          // pottype into potential type
          $("#potential-panel-content-potential-type").html("Rechteck");
          // val1: energy, val3: width, val4: x0, val2 hide
          // val1 energy
          $(divVal+"1").show();
          $(divVal+"1 label").html("Energie");
          $(divVal+"1 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.energy);
             }, 110);
          });
          $(divVal+"1 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"1").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"1").removeClass('is-invalid');
              potConfig.energy = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"1 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"1").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"1").removeClass('is-invalid');
                potConfig.energy = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
         // val5 alpha
          $(divVal+"5").show();
          $(divVal+"5 label").html("Alpha");
          $(divVal+"5 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.alpha);
             }, 110);
          });
          $(divVal+"5 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)&&val!=0) {
              $(divVal+"5").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"5").removeClass('is-invalid');
              potConfig.alpha = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"5 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)&&val!=0) {
                $(divVal+"5").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"5").removeClass('is-invalid');
                potConfig.alpha = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val3 width
          $(divVal+"3").show();
          $(divVal+"3 label").html("Breite");
          $(divVal+"3 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.width);
             }, 110);
          });
          $(divVal+"3 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val) || parseFloat(val) <= 0) {
              $(divVal+"3").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"3").removeClass('is-invalid');
              potConfig.width = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"3 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val) || parseFloat(val) <= 0) {
                $(divVal+"3").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"3").removeClass('is-invalid');
                potConfig.width = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val4 x0
          $(divVal+"4").show();
          $(divVal+"4 label").html("x<sub>0</sub>");
          $(divVal+"4 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.x0);
             }, 110);
          });
          $(divVal+"4 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"4").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"4").removeClass('is-invalid');
              potConfig.x0 = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"4 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"4").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"4").removeClass('is-invalid');
                potConfig.x0 = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
        }
        else if (potType == 'Gauss') {
          $("#potential-panel-content-potential-type").html("Gauss");
          // val1: energy, val3: width, val4: x0, val2 hide
          // val1 energy
          $(divVal+"1").show();
          $(divVal+"1 label").html("Energie");
          $(divVal+"1 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.energy);
             }, 110);
          });
          $(divVal+"1 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"1").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"1").removeClass('is-invalid');
              potConfig.energy = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"1 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"1").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"1").removeClass('is-invalid');
                potConfig.energy = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val3 sigma
          $(divVal+"3").show();
          $(divVal+"3 label").html("Sigma");
          $(divVal+"3 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.sigma);
             }, 110);
          });
          $(divVal+"3 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val) || parseFloat(val) <= 0) {
              $(divVal+"3").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"3").removeClass('is-invalid');
              potConfig.sigma = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"3 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val) || parseFloat(val) <= 0) {
                $(divVal+"3").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"3").removeClass('is-invalid');
                potConfig.sigma = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val4 x0
          $(divVal+"4").show();
          $(divVal+"4 label").html("x<sub>0</sub>");
          $(divVal+"4 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.x0);
             }, 110);
          });
          $(divVal+"4 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"4").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"4").removeClass('is-invalid');
              potConfig.x0 = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"4 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"4").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"4").removeClass('is-invalid');
                potConfig.x0 = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
        }
        else if (potType == 'Osci') {
          $("#potential-panel-content-potential-type").html("Oszillator");
          // val2: w, val5: x0, val3: xMin, val4 xMax
          // val2 w
          $(divVal+"2").show();
          $(divVal+"2 label").html("w");
          $(divVal+"2 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.w);
             }, 110);
          });
          $(divVal+"2 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"2").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"2").removeClass('is-invalid');
              potConfig.w = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"2 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"2").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"2").removeClass('is-invalid');
                potConfig.w = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val5 x0
          $(divVal+"5").show();
          $(divVal+"5 label").html("x<sub>0</sub>");
          $(divVal+"5 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.x0);
             }, 110);
          });
          $(divVal+"5 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"5").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"5").removeClass('is-invalid');
              potConfig.x0 = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"5 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"5").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"5").removeClass('is-invalid');
                potConfig.x0 = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val3 xMin
          $(divVal+"3").show();
          $(divVal+"3 label").html("x<sub>Min</sub>");
          $(divVal+"3 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.xMin);
             }, 110);
          });
          $(divVal+"3 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"3").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"3").removeClass('is-invalid');
              potConfig.xMin = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"3 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"3").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"3").removeClass('is-invalid');
                potConfig.xMin = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val4 xMax
          $(divVal+"4").show();
          $(divVal+"4 label").html("x<sub>Max</sub>");
          $(divVal+"4 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.xMax);
             }, 110);
          });
          $(divVal+"4 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"4").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"4").removeClass('is-invalid');
              potConfig.xMax = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"4 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"4").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"4").removeClass('is-invalid');
                potConfig.xMax = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
        } else if (potType == 'Personal') {
          $("#potential-panel-content-potential-type").html("Funktion");
          // val6 potFunc
          $(divVal+"6").show();
          $(divVal+"6 label").html("V(x)");
          $(divVal+"6 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder",potConfig.potFunc);
              $(self).val(potConfig.potFunc);
             }, 110);
          });
          $(divVal+"6 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            var x = 0;
            if (val == null) {

            }
            else if (isNaN(eval(val))) {
              $(divVal+"6").addClass('is-invalid');
            }
            else {
              $(divVal+"6").removeClass('is-invalid');
              potConfig.potFunc = val;
              self.buildUp();
              $(this).val(null);
            }
          });
          $(divVal+"6 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              var x = 0;
              if (val == null) {

              }
              else if (isNaN(eval(val))) {
                $(divVal+"6").addClass('is-invalid');
              }
              else {
                $(divVal+"6").removeClass('is-invalid');
                potConfig.potFunc = val;
                self.buildUp();
              }
              $(this).blur();    
              $(this).val(null);
            }
          });
          // val3 xMin
          $(divVal+"3").show();
          $(divVal+"3 label").html("x<sub>Min</sub>");
          $(divVal+"3 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.xMin);
             }, 110);
          });
          $(divVal+"3 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"3").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"3").removeClass('is-invalid');
              potConfig.xMin = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"3 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"3").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"3").removeClass('is-invalid');
                potConfig.xMin = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });
          // val4 xMax
          $(divVal+"4").show();
          $(divVal+"4 label").html("x<sub>Max</sub>");
          $(divVal+"4 input").focusin(function (){
            var self = this;
            setTimeout(function() {
              $(self).attr("placeholder", potConfig.xMax);
             }, 110);
          });
          $(divVal+"4 input").focusout(function (){
            $(this).attr("placeholder", "");
            var val = $(this).val();
            if (isNaN(val)) {
              $(divVal+"4").addClass('is-invalid');
            }
            else if(val) {
              $(divVal+"4").removeClass('is-invalid');
              potConfig.xMax = parseFloat(val);
              self.buildUp();
            }
          });
          $(divVal+"4 input").keypress(function(e){
            if(e.which == 13){
              $(this).attr("placeholder", "");
              var val = $(this).val();
              if (isNaN(val)) {
                $(divVal+"4").addClass('is-invalid');
              }
              else if(val) {
                $(divVal+"4").removeClass('is-invalid');
                potConfig.xMax = parseFloat(val);
                self.buildUp();
              }
              $(this).blur();    
            }
          });         
        }
        // display config content
        $("#potential-panel-content-config").removeClass('displayNone');
      
      $("#potential-panel-content-delete").unbind("click");
      $("#potential-panel-content-delete").click(function (){
        delete self._quantum._config.potential[id];
        self._quantum._config.potential = potential.filter(function(a){return typeof a !== 'undefined';});
        self.buildUp();
        makePotList();
        $("#potential-panel-content-title, #potential-panel-content-config, #potential-panel-content-type, #potential-panel-content-delete, #potential-panel-content-potential-type").addClass('displayNone');
      });
      
    });
    return;
  }
  makePotTypeList();
  makePotList();
  // click event potential element

};
// global plotting
var plotting; 
// global active animation config id
var globalActId;
/*  FIRST ACTIVATION OF PLOTTING
*   ---------------------------------------------------
*   1. screen specific content
*   2. import config from url
*   3. list animation click events
*/
$(document).ready(function() {
  var firstBuilderTriggerClick = false;
  var width = $( window ).width();
  var AnimationSize512px = false;
  var AnimationSize256px = false;
  if ( width <= 530 && !AnimationSize256px) {
    AnimationSize512px = false;
    AnimationSize256px = true;
    plotting = new Plotting('animation-container', 256, 145, 0xF0F4C3);
    $(".animation-card").css("width","256px");
    }
  else if (width > 530 && !AnimationSize512px){
    AnimationSize512px = true;
    AnimationSize256px = false;
    plotting = new Plotting('animation-container', 512, 290, 0xF0F4C3);
  }
  if ( width < 600 ) {
    $(".chapterTitle").addClass("mdl-typography--display-1");
    $(".sectionTitle").addClass("mdl-typography--headline");
  }
  else if (width < 1025) {
    $(".chapterTitle").addClass("mdl-typography--display-2");
    $(".sectionTitle").addClass("mdl-typography--headline");
  }
  else {
    $(".chapterTitle").addClass("mdl-typography--display-3");
    $(".sectionTitle").addClass("mdl-typography--display-1");
  }
  
  plotting._quantum = new Quantum1D().OperatorSplitting(); //PeriodicNC OperatorSplitting ZeroCN
  var lowQuality = false;
  $( window ).resize(function() {
    var width = $( window ).width();
    if ( width < 840 && !lowQuality) {
      lowQuality = true;
      var inactiveColor = 'mdl-color-text--blue-grey-200';
      var activeColor = 'mdl-color-text--indigo-500';
      $("#high-quality i").removeClass(activeColor).addClass(inactiveColor);
      plotting._quantum._config.length = 2048;
      plotting.buildUp();
    } else if (width >= 840) {
      lowQuality = false;
    }
  if ( width < 600 ){
      $(".chapterTitle").each(function (){
        if($(this).hasClass("mdl-typography--display-3"))
          $(this).removeClass("mdl-typography--display-3").addClass("mdl-typography--display-1");
        if($(this).hasClass("mdl-typography--display-2"))
          $(this).removeClass("mdl-typography--display-2").addClass("mdl-typography--display-1");
      });
      $(".sectionTitle").each(function (){
        if($(this).hasClass("mdl-typography--display-1"))
          $(this).removeClass("mdl-typography--display-1").addClass("mdl-typography--headline");
      });
  }
  else if ( width < 1025 ){
      $(".chapterTitle").each(function (){
        if($(this).hasClass("mdl-typography--display-1"))
          $(this).removeClass("mdl-typography--display-1").addClass("mdl-typography--display-2");
        if($(this).hasClass("mdl-typography--display-3"))
          $(this).removeClass("mdl-typography--display-3").addClass("mdl-typography--display-2");
      });
      $(".sectionTitle").each(function (){
        if($(this).hasClass("mdl-typography--display-1"))
          $(this).removeClass("mdl-typography--display-1").addClass("mdl-typography--headline");
      });
  }
  else {
      $(".chapterTitle").each(function (){
        if($(this).hasClass("mdl-typography--display-1"))
          $(this).removeClass("mdl-typography--display-1").addClass("mdl-typography--display-3");
        if($(this).hasClass("mdl-typography--display-2"))
          $(this).removeClass("mdl-typography--display-2").addClass("mdl-typography--display-3");
      });
      $(".sectionTitle").each(function (){
        if($(this).hasClass("mdl-typography--headline"))
          $(this).removeClass("mdl-typography--headline").addClass("mdl-typography--display-1");
      });
  }
    var AnimationSize1024px = false;
    if($(".animation-card").width() > 512) {
     AnimationSize1024px = true;    
     AnimationSize512px = false;
     AnimationSize256px = false;
    }
    if ( width <= 530 && !AnimationSize256px) {
      $("#animation-container-information").css("display","none");
      AnimationSize512px = false;
      AnimationSize256px = true;
      $(".animation-card").css("width","256px");
      plotting._renderer.resize(256, 145);
      plotting.width = 256;
      plotting.height = 145;
      plotting.buildUp();
    }
    else if (width > 530 && (width < 1320 && (!AnimationSize512px || AnimationSize1024px) || AnimationSize256px)) {
      AnimationSize512px = true;
      AnimationSize256px = false;
      $("#page-content-simulation-tool").css("max-width","860px");
      $(".animation-card").css("width","512px");
      plotting._renderer.resize(512, 290);
      plotting.width = 512;
      plotting.height = 290;
      plotting.buildUp();
      // crop landscape icon off
      var inactiveColor = 'mdl-color-text--blue-grey-200';
      var activeColor = 'mdl-color-text--indigo-500';
      $("#crop_landscape i").removeClass(activeColor).addClass(inactiveColor);
    }
  });
    // ======================
    // Import Config from URL
    // ======================
      var url = decodeURI(window.location.href);
      var config = [];
      if (url.split("#")[1]) {
        config = url.split("#")[1].split("&");
      }
      if(config.length>1) {
        var importConfig = {
          id: StandardConfigs.length,
          name: config[0],
         config: {
          grid: {
            xmin: parseFloat(config[1]),
            xmax: parseFloat(config[2])
          },
          maxEnergy: parseFloat(config[10]),
          dt: 1e-2,
          wavefactor: 0, // these factors are calculated later
          potfactor: 0,
          momentumfactor: 0,
          length: parseFloat(config[3]),
          density: 2,
          momentum: true,
          potColor: {
            pos: 0x8D6E63,
            neg: 0xD7CCC8
          },
          potential: [
          ],
          wavefunction: 
            {
              energy: parseFloat(config[4]),
              mass: parseFloat(config[5])*M_EL,
              sigma: parseFloat(config[6]),
              x0: parseFloat(config[7]),
              color: {
                E: true,
                xE: 0x9E9D24,
                x0: 0x795548, // wave color for E = 0
                k: 0xe57373,
                line: 0xD4E157
              }
            }
        },
    design: {
      xLabel: {
        plot: true,
        delta: parseFloat(config[8]),
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: parseFloat(config[9]),
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }       
      };
      // add potentials 
      var potConfigurations = config[11].split("%");
      if (potConfigurations!="") {
        for (var i = 0; i < potConfigurations.length; i++) {
          var importPot = potConfigurations[i].split("_");
          var id =  parseFloat(importPot[0]);
          var potConf;
          if (id == 1) {
            potConf = {
              type: 'Rect',
              name: importPot[1],
              energy: parseFloat(importPot[2]),
              width: parseFloat(importPot[3]),
              x0: parseFloat(importPot[4]),
              alpha: parseFloat(importPot[5])
            };
          }
          else if (id == 2) {
            potConf = {
              type: 'Osci',
              name: importPot[1],
              w: parseFloat(importPot[2]),
              x0: parseFloat(importPot[3]),
              xMin: parseFloat(importPot[4]),
              xMax: parseFloat(importPot[5])
            };
          }
          else if (id == 3) {
            potConf = {
              type: 'Gauss',
              name: importPot[1],
              energy: parseFloat(importPot[2]),
              sigma: parseFloat(importPot[3]),
              x0: parseFloat(importPot[4])
            };
          }
          else if (id == 4) {
            potConf = {
              type: 'Personal',
              name: importPot[1],
              potFunc: importPot[2],
              xMin: parseFloat(importPot[3]),
              xMax: parseFloat(importPot[4])
            };          
          }
          importConfig.config.potential.push(potConf);
        }
      }
      StandardConfigs[0] = importConfig;
    }

    // =======================================================
    // STANDARD CONFIGS TO VIEW LIST SETTING TAB (#list-panel)
    // =======================================================
      var listPanel = $('#list-panel');
      var listPanelContent = "<div class='panelTitle mdl-color-text--lime-800' style='margin-top: 20px;'>Vordefinierte Simulationen</div>";
      var listPanelElements = "";
      listPanelContent += "<ul>";
      for (var i = 0; i < StandardConfigs.length; i++) {
        listPanelContent += "<li class='configList' id='config-" + i + "'><button class='mdl-button mdl-js-button mdl-js-ripple-effect mdl-color-text--blue-grey-600'>" + StandardConfigs[i].name + "</button></li>";
        if ( i == 0)
          listPanelElements += "#config-" + i;
        else
          listPanelElements += " , #config-" + i;
      }
      listPanelContent += "</ul>";
      listPanel.html(listPanelContent);
      // onclick event buil up list animation
      $(listPanelElements).click(function () {
        $("#potential-panel-content-title, #potential-panel-content-config, #potential-panel-content-delete, #potential-panel-content-type, #potential-panel-content-potential-type").addClass('displayNone');
        $("#animation-container-information").css("display","none");
        var id = $(this).attr("id").split("-")[1];
        globalActId = id;
        plotting._quantum._config = StandardConfigs[id].config;
        plotting._quantum._design = StandardConfigs[id].design;
        $('#animation-video-title-name').html(StandardConfigs[id].name);
        var inactiveColor = 'mdl-color-text--blue-grey-200';
       var activeColor = 'mdl-color-text--indigo-500';
        var videoIcons = '#stop i, #pause i, #playArrow i, #fast_forward i, #fast_rewind i';
        // get design and k options
        var design = plotting._quantum._design;
        var config = plotting._quantum._config;
        var activeOptions = new Array();
        var inactiveOptions = new Array();
        if (design.xLabel.plot) 
          activeOptions.push('#border_bottom i');
        else
          inactiveOptions.push('#border_bottom i');

        if (design.yLabel.plot)
          activeOptions.push('#border_left i');
        else
          inactiveOptions.push('#border_left i');

        if (design.grid) {
          activeOptions.push('#grid_plot i');
          $("#grid_plot i").html("grid_on");
        }
        else {
          inactiveOptions.push('#grid_plot i');
          $("#grid_plot i").html("grid_off");
        }

        if (config.momentum)
          activeOptions.push("#kRoomOption i");
        else
          inactiveOptions.push("#kRoomOption i");

        if(Math.log2(config.length) >= 13)
          activeOptions.push("#high-quality i");
        else
          inactiveOptions.push("#high-quality i");

        $(activeOptions.toString()).removeClass(inactiveColor).addClass(activeColor);
        $(inactiveOptions.toString()).removeClass(activeColor).addClass(inactiveColor);
        $(videoIcons).removeClass(activeColor).addClass(inactiveColor);
        $("#stop i").removeClass(inactiveColor).addClass(activeColor);
        //
        var width = $( window ).width();
        if ( width < 840) {
          var inactiveColor = 'mdl-color-text--blue-grey-200';
          var activeColor = 'mdl-color-text--indigo-500';
          plotting._quantum._config.length = 2048;
          $("#high-quality i").removeClass(activeColor).addClass(inactiveColor);
        }
        // make y range default
        if (!plotting._quantum._config)
          plotting._quantum._config.maxEnergy = false;        
        plotting.buildUp();
        // fill informations into tabs: settings-, psi-, potential-panel
        plotting.SettingsCard();
        // tablet/smartphone navigation
        if (!firstBuilderTriggerClick) {
          firstBuilderTriggerClick = true;
          plotting.TabletSmartphoneNavi();
        }
      });
      if(StandardConfigs[0].name == "Individuell")
        $("#config-3").trigger("click");
      else 
        $("#config-0").trigger("click");
  // animation information behaviour
  $("#start-information").click(function(){
    $("#animation-container-information").css("display","block");
  });
  $("#cancle-information").click(function(){
    $("#animation-container-information").css("display","none");
  });
});
/*  TABLETSMARTPHONENAVI
*   ---------------------------------------------------
*   1. navigation behaviour of animation without
*      settingcard
*/
Plotting.prototype.TabletSmartphoneNavi = function () {
  var self = this;
  // Tablet/Smartphone Previous/Next-Config Buttons
  $("#skip_previous").click(function (){
    var title = $("#animation-video-title-name").html();
    var objConfig = StandardConfigs.filter(function( obj ) {
      return obj.name == title;
    });
    var id = objConfig[0].id;
    var arrayLength = StandardConfigs.length;
    var newId = ((id-1) % arrayLength >= 0) ? (id-1) % arrayLength : arrayLength - 1 ;
    $("#config-"+newId).trigger("click");
  });
  $("#skip_next").click(function (){
    var title = $("#animation-video-title-name").html();
    var objConfig = StandardConfigs.filter(function( obj ) {
      return obj.name == title;
    });
    var id = objConfig[0].id;
    var arrayLength = StandardConfigs.length;
    var newId = (id+1) % arrayLength;
    $("#config-"+newId).trigger("click");
  });
};
/*  VIDEOCONTROLPANEL
*   ---------------------------------------------------
*   click events of control panel icons
*/
Plotting.prototype.VideoControlPanel = function () {
  var self = this;
  var inactiveColor = 'mdl-color-text--blue-grey-200';
  var activeColor = 'mdl-color-text--indigo-500';
  var inactiveClass = 'video-control-inactive';
  var activeClass = 'video-control-active';
  var button = '#animation-video-control button';
  var videoIcons = '#stop i, #pause i, #playArrow i, #fast_forward i, #fast_rewind i';
  $(button + ' .'+inactiveClass).addClass(inactiveColor);
  $(button + ' .'+activeClass).addClass(activeColor);
  // IF WINDOWS SIZE ....
  $(button + ' .video-control-hq').addClass(activeColor);
  $(button).click(function (){
    var icon = $(this).find('i');
    var action = icon.html();
    if (action == 'high_quality') {
      if(icon.hasClass(activeColor)) {
        icon.removeClass(activeColor).addClass(inactiveColor);
        self._quantum._config.length = 2048;
      }
      else {
        icon.removeClass(inactiveColor).addClass(activeColor);
        self._quantum._config.length = 8192;
      }
      $(videoIcons).removeClass(activeColor + ' ' + activeClass).addClass(inactiveColor + ' ' + inactiveClass);
      $('#stop i').removeClass(inactiveColor + ' ' + inactiveClass).addClass(activeClass + ' ' + activeColor);
    } else if (action == 'crop_landscape') {
      if(icon.hasClass(activeColor)) {
        icon.removeClass(activeColor).addClass(inactiveColor);
        $("#page-content-simulation-tool").css("max-width","860px");
        $(".animation-card").css("width","512px");
        self._renderer.resize(512, 290);
        self.width = 512;
        self.height = 290;      
      }
      else if($(window).width()>=1320) {
        icon.removeClass(inactiveColor).addClass(activeColor);
        $("#page-content-simulation-tool").css("max-width","1400px");
        $(".animation-card").css("width","1024px");
        self._renderer.resize(1024, 400);
        self.width = 1024;
        self.height = 400;          
      }
      else {
      }
    } else if (action == 'K' || action.split('_')[0] == 'grid' || action.split('_')[0] == 'border') {
      if(icon.hasClass(activeColor)) 
        icon.removeClass(activeColor).addClass(inactiveColor);      
      else 
        icon.removeClass(inactiveColor).addClass(activeColor);

      if (action == 'grid_on')
        icon.html('grid_off');
      else if (action == 'grid_off')
        icon.html('grid_on');

      // video control to stop
      if(action != 'K') {
        $(videoIcons).removeClass(activeColor + ' ' + activeClass).addClass(inactiveColor + ' ' + inactiveClass);
        $('#stop i').removeClass(inactiveColor + ' ' + inactiveClass).addClass(activeClass + ' ' + activeColor);        
      }
    } else {
      $(videoIcons).removeClass(activeColor + ' ' + activeClass).addClass(inactiveColor + ' ' + inactiveClass);
      icon.removeClass(inactiveColor + ' ' + inactiveClass).addClass(activeColor + ' ' + activeClass);
    }
    switch(action) {
      case 'pause':
        self.stop();
        break;
      case 'play_arrow':
        self._quantum._config.dt = 1e-2;
        self.newExpTPValues();
        break;
      case 'stop':
        self._quantum._config.dt = 1e-2;
        self.buildUp();
        break;
      case 'fast_rewind':
        self._quantum._config.dt /= 2;
        self.newExpTPValues();
        break;
      case 'fast_forward':
        self._quantum._config.dt *= 2;
        self.newExpTPValues();
        break;
      case 'fast_forward':
        self._quantum._config.dt *= 2;
        self.newExpTPValues();
        break;
      case 'high_quality':
        self._quantum._config.dt = 1e-2;
        self.buildUp();
        break;
      case 'crop_landscape':
        self.buildUp();
        break;
      case 'K':
        var momentum = self._quantum._config.momentum;
        self._quantum._config.momentum = (momentum) ? false : true;
        if (!self._running && self._time == 0)
          self.buildUp();
        break;
      case 'border_bottom':
        var plot = self._quantum._design.xLabel.plot;
        self._quantum._design.xLabel.plot = (plot) ? false : true;
        var border = ($('#animation-container').css("border-bottom-width")=='0px') ? '1px' : '0px';
        $('#animation-container').css("border-bottom-width", border);
        self.buildUp();
        break;
      case 'border_left':
        var plot = self._quantum._design.yLabel.plot;
        self._quantum._design.yLabel.plot = (plot) ? false : true;
        self.buildUp();
        break;
      case 'grid_on':
        self._quantum._design.grid.plot = false;
        self.buildUp();
        break;
      case 'grid_off':
        self._quantum._design.grid.plot = true;
        self.buildUp();
        break;
      default:
        console.log("Say Whaaaaaat?!");
    } 
  });
};
/*  STANDARDCONFIGS
*   ---------------------------------------------------
*   1. standard configs
*   2. if url config exists -> config[0] replaced
*/
var StandardConfigs = [
  {
    id: 0,
    name: 'Individuell',
    config: {
      grid: {
        xmin: -25,
        xmax: 25
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      ],
      wavefunction: 
        {
          energy: 10,
          mass: M_EL,
          sigma: 2,
          x0: 0,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 5,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 1,
    name: 'Wellenzahl-Raum',
    config: {
      grid: {
        xmin: -75,
        xmax: 75
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      ],
      wavefunction: 
        {
          energy: 1.9049910404714674,
          mass: M_EL,
          sigma: 1,
          x0: -30,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 15,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
{
    id: 2,
    name: 'Freies Wellenpaket',
    config: {
      grid: {
        xmin: -75,
        xmax: 75
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      ],
      wavefunction: 
        {
          energy: 100,
          mass: M_EL,
          sigma: 2,
          x0: 0,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 15,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 5,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 3,
    name: 'Tunneleffekt',
    config: {
      grid: {
        xmin: -75,
        xmax: 75
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      {
        type: 'Rect',
        name: 'Potentialbarriere',
        energy: 100,
        width: 0.5,
        x0: 15,
        alpha: 0.1
      }
      ],
      wavefunction: 
        {
          energy: 75,
          mass: M_EL,
          sigma: 8,
          x0: -35,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 15,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 4,
    name: 'Potentialsenke',
    config: {
      grid: {
        xmin: -75,
        xmax: 75
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      {
        type: 'Rect',
        name: 'Potentialsenke',
        energy: -400,
        width: 2,
        x0: 10,
        alpha: 0.1
      }
      ],
      wavefunction: 
        {
          energy: 50,
          mass: M_EL,
          sigma: 10,
          x0: -35,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 15,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 8,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 5,
    name: 'Harm. Oszillator',
    config: {
      grid: {
        xmin: -75,
        xmax: 75
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      {
        type: 'Osci',
        name: 'Oszillator',
        w: 0.5,
        x0: 0,
        xMin: -75,
        xMax: 75
      }
      ],
      wavefunction: 
        {
          energy: 120,
          mass: M_EL,
          sigma: 3,
          x0: 0,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 15,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 6,
    name: 'Kohärenter Zust.',
    config: {
      grid: {
        xmin: -75,
        xmax: 75
      },
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      {
        type: 'Osci',
        name: 'Oszillator',
        w: 0.5,
        x0: 0,
        xMin: -75,
        xMax: 75
      }
      ],
      wavefunction: 
        {
          energy: 329.9287406392501,
          mass: M_EL,
          sigma: 1.521628312222495,
          x0: 0,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 15,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 7,
    name: 'Quasistation. Zust.',
    config: {
      grid: {
        xmin: -70,
        xmax: 70
      },
      maxEnergy: 140,
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
      {
        type: 'Rect',
        name: 'Linke Barriere',
        energy: 100,
        width: 0.5,
        x0: 5,
        alpha: 0.1
      },
      {
        type: 'Rect',
        name: 'Rechte Barriere',
        energy: 100,
        width: 0.5,
        x0: 10,
        alpha: 0.1
      }
      ],
      wavefunction: 
        {
          energy: 28.725,
          mass: M_EL,
          sigma: 10,
          x0: -30,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 10,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  },
  {
    id: 8,
    name: 'Lennard-Jones',
    config: {
      grid: {
        xmin: -70,
        xmax: 70
      },
      maxEnergy: 250,
      dt: 1e-2,
      wavefactor: 0, // these factors are calculated later
      potfactor: 0,
      momentumfactor: 0,
      length: 8192,
      density: 2,
      momentum: true,
      potColor: {
        pos: 0x8D6E63,
        neg: 0xD7CCC8
      },
      potential: [
        {
          type: 'Personal',
          name: 'Lennard-Jones-Pot.',
          potFunc: '1000*(Math.pow((x+110)*0.02,-12)-Math.pow((x+110)*0.02,-6))',
          xMin: -70,
          xMax: 70
        }
      ],
      wavefunction: 
        {
          energy: 0.0761996416188587,
          mass: M_EL,
          sigma: 5,
          x0: -40,
          color: {
            E: true,
            xE: 0x9E9D24,
            x0: 0x795548, // wave color for E = 0
            k: 0xe57373,
            line: 0xD4E157
          }
        }
    },
    design: {
      xLabel: {
        plot: true,
        delta: 10,
        color: {
          label: 0x4E342E,
          axis: 0xFFF8E1
        },
        fontsize: 10
      },
      yLabel: {
        plot: true,
        sampling: 6,
        color: {
          label: 0x4E342E,
          axis: 0x4E342E,
          vert: 0x4E342E
        },
        fontsize: 10
      },
      grid: {
        plot: true,
        color: 0xE6EE9C
      }
    }
  }
];
/*
$(document).ready(function() {
  // choose config quasistationärer zustand
  $("#config-7").trigger("click");
  // k off
  $("#kRoomOption").trigger("click");
  //$("#crop_landscape").trigger("click");
  plotting._quantum._config.wavefunction.energy = 80;
  plotting.buildUp();
  plotting.newExpTPValues();
});
*/