// defining complex numbers
/*
basic complex number arithmetic from
http://rosettacode.org/wiki/Fast_Fourier_transform#Scala
*/
function Complex(re, im)
{
	this.re = re || 0.0;
	this.im = im || 0.0;
}
Complex.prototype.add = function(other, dst)
{
	dst.re = this.re + other.re;
	dst.im = this.im + other.im;
	return dst;
};
Complex.prototype.sub = function(other, dst)
{
	dst.re = this.re - other.re;
	dst.im = this.im - other.im;
	return dst;
};
Complex.prototype.mul = function(other, dst)
{
	//cache re in case dst === this
	var r = this.re * other.re - this.im * other.im;
	dst.im = this.re * other.im + this.im * other.re;
	dst.re = r;
	return dst;
};
Complex.prototype.div = function(other, dst)
{
	//cache re in case dst === this
	var abs2 = other.re * other.re + other.im * other.im;
	var r = this.re * other.re + this.im * other.im;
	dst.im = (this.im * other.re - this.re * other.im ) / abs2;
	dst.re = r / abs2;
	return dst;
};
Complex.prototype.cexp = function(dst)
{
	var er = Math.exp(this.re);
	dst.re = er * Math.cos(this.im);
	dst.im = er * Math.sin(this.im);
	return dst;
};
// calc abs |.|^2
Complex.prototype.abs2 = function()
{
	var abs2 = this.re * this.re + this.im * this.im;
	return abs2;
};
