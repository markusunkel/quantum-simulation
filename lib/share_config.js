// create short link of configuration
$(document).ready(function (){
  $("#share-config").click(function(){
    //$("#pause").trigger("click");
    $("#animation-container-share-content").css("display","block");
  });
  $("#cancle-share").click(function(){
    $("#animation-container-share-content").css("display","none");
  });
  $("#share-make-link").click(function(){
    var name = $("#share-sim-name").val();
    if(name==="")
      name = "Eigene Simulation";
    //get recent config & design
    var general = StandardConfigs[globalActId];
    var config = general.config;
    var design = general.design;
    // create config url appendix
    var configUrl = "";
    configUrl += name + "&";
    configUrl += config.grid.xmin + "&";
    configUrl += config.grid.xmax + "&";
    configUrl += config.length + "&";
    configUrl += config.wavefunction.energy + "&";
    configUrl += config.wavefunction.mass/M_EL + "&";
    configUrl += config.wavefunction.sigma + "&";
    configUrl += config.wavefunction.x0 + "&";
    configUrl += design.xLabel.delta + "&";
    configUrl += design.yLabel.sampling + "&";
    configUrl += config.maxEnergy + "&";
    // add potconfigs
    for (var i = 0; i < config.potential.length; i++) {
      if (i>0) {
        configUrl += "%";
      }
      var potConfig = config.potential[i];
      var type = potConfig.type;
      if (type == "Rect") {
        configUrl += "1_";
        configUrl += potConfig.name + "_";
        configUrl += potConfig.energy + "_";
        configUrl += potConfig.width + "_";
        configUrl += potConfig.x0 + "_";
        configUrl += potConfig.alpha;
      }
      else if (type == "Osci") {
        configUrl += "2_";
        configUrl += potConfig.name + "_";
        configUrl += potConfig.w + "_";
        configUrl += potConfig.x0 + "_";
        configUrl += potConfig.xMin + "_";
        configUrl += potConfig.xMax;
      }
      else if (type == "Gauss") {
        configUrl += "3_";
        configUrl += potConfig.name + "_";
        configUrl += potConfig.energy + "_";
        configUrl += potConfig.sigma + "_";
        configUrl += potConfig.x0;
      }
      else if (type == "Personal") {
        configUrl += "4_";
        configUrl += potConfig.name + "_";
        configUrl += potConfig.potFunc + "_";
        configUrl += potConfig.xMin + "_";
        configUrl += potConfig.xMax;
      }
    }
    window.location.hash = encodeURI(configUrl);
    var newUrl = window.location.href;
    $("#share-link").val(newUrl);
    $("#share-link").css("display","block");
    $("#share-link").select();
  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
  });
});
